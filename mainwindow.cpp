#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "portpickerdialog.h"

#include <QMessageBox>
#include <QDebug>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui {new Ui::MainWindow},
    m_statusLabel {new QLabel(this)}
{
    m_ui->setupUi(this);
    statusBar()->addWidget(m_statusLabel);

    bool connectOk = true;
    connectOk &= static_cast<bool>(connect(&m_port, SIGNAL(errorOccurred(QSerialPort::SerialPortError)),
                                           this, SLOT(OnSerialPortErrorOccurred(QSerialPort::SerialPortError))));
    connectOk &= static_cast<bool>(connect(&m_port, SIGNAL(readyRead()),
                                           this, SLOT(OnSerialPortReadyRead())));
    Q_ASSERT(connectOk);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::on_action_openPort_triggered()
{
    if(m_port.isOpen())
        m_port.close();

    PortPickerDialog portPicker;

    // Частоты, указанные на с. 10 (на случай, если среди них есть те которые не поддерживаются ОС по умолчанию)
    portPicker.AddMoreBaudRates({4800, 9600, 19200, 38400});

    // TODO: заменить на параметры, на которые настроен блок
    portPicker.SetSettings({38400, QSerialPort::Data8, QSerialPort::NoParity, QSerialPort::OneStop, QSerialPort::NoFlowControl});

    if(portPicker.exec() != QDialog::Accepted) // пользователь нажал отмену
        return;

    QSerialPortInfo chosenPort;
    if(portPicker.GetPortInfo(&chosenPort))
    {
        QMessageBox::warning(nullptr, tr("Ошибка"), tr("Выберите порт."));
        return;
    }

    m_port.setPort(chosenPort);
    m_port.setReadBufferSize(1000000);

    QString culprit;
    if(OpenAndTune(portPicker.GetSettings(), &culprit))
    {
        qWarning() << culprit << ": " << m_port.error() << ": " << m_port.errorString();
        QMessageBox::warning(nullptr, tr("Ошибка"), culprit + ": " + m_port.errorString());
        return;
    }

    qDebug() << "Port " << chosenPort.portName() << " opened with descriptor " << m_port.handle();
}

bool MainWindow::OpenAndTune(const SerialPortSettings &settings, QString *culprit)
{
    if(!m_port.open(QIODevice::ReadWrite))
    {
        (*culprit) = "open";
        return true;
    }
    m_bytesRead = 0;

    if(settings.TunePort(&m_port, culprit))
        return true;

    return false;
}

void MainWindow::OnSerialPortErrorOccurred(QSerialPort::SerialPortError error)
{
    if(error != QSerialPort::NoError)
        qWarning() << "Port error " << error;
}

void MainWindow::OnSerialPortReadyRead()
{
    forever
    {
        const qint64 bytesAvailable {m_port.bytesAvailable()};
        if(bytesAvailable < 1)
            break;

        static constexpr int bufSize {4096};
        quint8 buf[bufSize] {};

        const qint64 bytesToRead {qMin<qint64>(bufSize, bytesAvailable)};
        {
            const qint64 ret {m_port.read(reinterpret_cast<char*>(buf), bytesToRead)};
            if(ret != bytesToRead)
            {
                qWarning() << ret << "!=" << bytesToRead << ": " << m_port.error() << ": " << m_port.errorString();
                m_port.close();
                return;
            }
            m_bytesRead += ret;
        }

        for(int idx = 0; idx < bytesToRead; idx ++)
        {
            if(m_parser.Feed(buf[idx]))
            {
                quint8 messageId {0};
                const QVector<quint8> payload {m_parser.GetPayload(&messageId)};
                qDebug() << "Got message " << messageId << ", " << payload.size() << " B payload";
            }
        }
    }

    m_statusLabel->setText(tr("%1 байт получено").arg(m_bytesRead));
}
